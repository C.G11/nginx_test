
const https = require('https')
const config = require('./config.json')
const fs = require('fs');

const options = {
  key: fs.readFileSync('certs/host.key'),
  cert: fs.readFileSync('certs/host.cert')
};

const server = https.createServer(options,function (req, res) {
        var body = '';

        req.on('data', function (data) {
                body += data;
                // Too much POST data, kill the connection! // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
                if (body.length > 1e6) {
                        req.connection.destroy();
                }
                console.log(req)
        });
        if (req.url === '/' && req.method === "GET") {
                res.writeHead(200);
                res.end('hello world\n');
        }

});


server.listen(config.port)
console.log('Server started on port ', config.port)
